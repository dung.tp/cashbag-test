package com.testapptest.test.application

import android.app.Application
import android.content.Context
import com.testapptest.test.di.ActivityModule
import com.testapptest.test.di.ConfigureModule
import dagger.Module
import dagger.Provides
import dagger.android.support.AndroidSupportInjectionModule

@Module(
    includes = [
        AndroidSupportInjectionModule::class,
        ActivityModule::class,
        ConfigureModule::class
    ]
)
class AppModule {

    @Provides
    fun provideContext(application: Application): Context {
        return application.applicationContext
    }
}