package com.testapptest.test.base.view

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.ResolveInfo
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.testapptest.test.extension.hideKeyboard
import com.testapptest.test.base.viewmodel.BaseViewModel
import com.testapptest.test.customize.dialog.LoadingProgress
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject



abstract class BaseActivity<V : ViewDataBinding, T : BaseViewModel> : DaggerAppCompatActivity(),
    IBaseActivity<T> {

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProvider.Factory

    lateinit var mViewModel: T


    override val viewContext: Context
        get() = this

    override fun getViewModel(): T {
        return mViewModel
    }

    private var loadingProgress: LoadingProgress? = null
    private var handelClickBack: HandelClickBack? = null

    protected lateinit var dataBinding: V

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        onStartAnimateTransaction()
        onChangeStatusBarColor()
        mViewModel = ViewModelProviders.of(this, viewModelProviderFactory).get(getModelClass())
        dataBinding = DataBindingUtil.setContentView(this, getLayoutRes())
        dataBinding.lifecycleOwner = this
        initViewModel()
        initView()
    }

    open fun onStartAnimateTransaction() {
        if (!intent.hasExtra("SkipAnimation")) {
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }
    }

    override fun onChangeStatusBarColor() {}

    override fun onFullScreen() {
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        setWindowFlag(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false)
        window.statusBarColor = Color.TRANSPARENT
    }

    private fun setWindowFlag(bits: Int, on: Boolean) {
        val win = window
        val winParams = win.attributes
        if (on) {
            winParams.flags = winParams.flags or bits
        } else {
            winParams.flags = winParams.flags and bits.inv()
        }
        win.attributes = winParams
    }

    override fun onPause() {
        super.onPause()
        if (isFinishing) {
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        onCheckNewIntent(intent)
    }

    /**
     * Init [View] components here. Such as set adapter for [RecyclerView], set listener
     * or anything else
     */
    @SuppressLint("SetTextI18n")
    override fun initView() {
        loadingProgress = LoadingProgress(this)
    }

    override fun initViewModel() {
        mViewModel.apply {
            lifecycle.addObserver(this as LifecycleObserver)
        }
    }

    override fun getToolbarTitle(): String? = null

    override fun onCheckNewIntent(intent: Intent?) {}

    override fun onEditTextChangedCallback(value: String?) {}

    override fun showLoadingDialog() {
        if (loadingProgress == null) {
            loadingProgress = LoadingProgress(this)
        }
        loadingProgress?.let {
            if (!it.isShowing) {
                it.show()
            }
        }
    }

    override fun dismissLoadingDialog() {
        try {
            loadingProgress?.let {
                it.dismiss()
                loadingProgress = null
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }



    override fun getCurrentFragment(id: Int): BaseFragment<*, *>? {
        supportFragmentManager.findFragmentById(id)?.childFragmentManager?.fragments?.let {
            if (it.isNotEmpty()) {
                return it[0] as? BaseFragment<*, *>
            }
        }
        return null
    }

    override fun getFragments(id: Int): MutableList<Fragment>? {
        return supportFragmentManager.findFragmentById(id)?.childFragmentManager?.fragments
    }

    override fun getNavId(): Int? = null

    override fun onHandleBackPressed() {
        handelClickBack?.onBack()
        onBackPressed()
    }

    fun handelBackListener(listener: HandelClickBack) {
        handelClickBack = listener
    }

    interface HandelClickBack {
        fun onBack()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            loadingProgress?.let {
                if (it.isShowing) {
                    it.dismiss()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    private var isClick: Boolean = false
    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        onDispatchTouchEvent()
        when (ev?.action) {
            MotionEvent.ACTION_DOWN -> {
                isClick = true
            }
            MotionEvent.ACTION_CANCEL, MotionEvent.ACTION_UP -> {
                if (isClick) {
                    val v = currentFocus
                    if (v != null && v is EditText && !v.javaClass.name.startsWith("android.webkit.")) {
                        val scrcoords = IntArray(2)
                        v.getLocationOnScreen(scrcoords)
                        val x = ev.rawX + v.getLeft() - scrcoords[0]
                        val y = ev.rawY + v.getTop() - scrcoords[1]
                        if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom()
                        ) {
                            v.clearFocus()
                            Handler().postDelayed({
                                if (currentFocus !is EditText) {
                                    v.hideKeyboard(this)
                                }
                            }, 50)
                        }
                    }
                }
            }

            //dung.tp rem to fix: keyboard not hide when click out of EditText
//            MotionEvent.ACTION_MOVE -> {
//                isClick = false
//            }
        }
        return super.dispatchTouchEvent(ev)
    }

    override fun onDispatchTouchEvent() {
    }




    private fun addIntentsToList(
        context: Context,
        list: MutableList<Intent>,
        intent: Intent
    ): MutableList<Intent> {
        val resInfo: List<ResolveInfo> =
            context.packageManager.queryIntentActivities(intent, 0)
        for (resolveInfo in resInfo) {
            val packageName = resolveInfo.activityInfo.packageName
            val targetedIntent = Intent(intent)
            targetedIntent.setPackage(packageName)
            list.add(targetedIntent)
        }
        return list
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            data?.data?.apply {
                onUriResult(this, requestCode)
                return
            }

            data?.extras?.get("data")?.apply {
                onBitmapResult(this as? Bitmap, requestCode)
            }
        }
    }

    open fun onUriResult(uri: Uri, requestCode: Int) {}

    open fun onBitmapResult(bitmap: Bitmap?, requestCode: Int) {}

    open fun onChooseImage(requestCode: Int, data: Intent?) {}

}