package com.testapptest.test.base.view

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.ResolveInfo
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.ScrollView
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.testapptest.test.extension.gone
import com.testapptest.test.extension.visible
import com.testapptest.test.R
import com.testapptest.test.base.viewmodel.BaseViewModel
import com.testapptest.test.base.viewmodel.IBaseViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject

abstract class BaseFragment<V : ViewDataBinding, T : BaseViewModel> : DaggerFragment(),
    IBaseFragment<T> {

    private var isShowKeyboard: Boolean = false
    private var onGlobalLayoutListener: ViewTreeObserver.OnGlobalLayoutListener? = null
    override val navController by lazy { findNavController() }

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProvider.Factory

    lateinit var mViewModel: T

    protected lateinit var dataBinding: V


    private var spaceView: View? = null
    private var scrollView: ScrollView? = null

    override val viewContext: Context
        get() = requireContext()

    override fun getViewModel(): T {
        return mViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProviders.of(this, viewModelProviderFactory).get(getModelClass())
        lifecycle.addObserver(mViewModel as LifecycleObserver)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSpaceView(view)
        dataBinding.lifecycleOwner = this
        initViewModel()
        initView()
        initEvent()
    }

    private fun initSpaceView(view: View) {
        spaceView = view.findViewWithTag(getString(R.string.tag_space_view))
        scrollView = view.findViewWithTag(getString(R.string.tag_scroll_view))
        dataBinding.root.post {

        }
    }

    override fun initView() {

    }

    override fun getToolbarTitle(): String? = null

    open fun initEvent() {}

    override fun onEditTextChangedCallback(value: String?) {}

    override fun getCurrentFragment(id: Int): BaseFragment<*, *>? {
        kotlin.runCatching {
            childFragmentManager.findFragmentById(id)?.childFragmentManager?.fragments?.let {
                if (it.isNotEmpty()) {
                    return it[0] as? BaseFragment<*, *>
                }
            }
        }
        return null
    }

    override fun getFragments(id: Int): MutableList<Fragment>? {
        kotlin.runCatching {
            childFragmentManager.findFragmentById(id)?.childFragmentManager?.fragments
        }
        return null
    }




    private fun addIntentsToList(
        context: Context,
        list: MutableList<Intent>,
        intent: Intent
    ): MutableList<Intent> {
        val resInfo: List<ResolveInfo> =
            context.packageManager.queryIntentActivities(intent, 0)
        for (resolveInfo in resInfo) {
            val packageName = resolveInfo.activityInfo.packageName
            val targetedIntent = Intent(intent)
            targetedIntent.setPackage(packageName)
            list.add(targetedIntent)
        }
        return list
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            data?.data?.apply {
                onUriResult(this, requestCode)
                return
            }

            data?.extras?.get("data")?.apply {
                onBitmapResult(this as? Bitmap, requestCode)
            }
        }
    }

    open fun onUriResult(uri: Uri, requestCode: Int) {}

    open fun onBitmapResult(bitmap: Bitmap?, requestCode: Int) {}

    open fun onChooseImage(requestCode: Int, data: Intent?) {}

    override fun showLoadingDialog() {
        getParentActivity().showLoadingDialog()
    }

    override fun dismissLoadingDialog() {
        getParentActivity().dismissLoadingDialog()
    }



    override fun onScrollToTop() {

    }

    override fun navigate(id: Int, bundle: Bundle?) {
        kotlin.runCatching {
            navController.navigate(id, bundle)
        }
    }

    override fun navigate(id: Int) {
        kotlin.runCatching {
            navController.navigate(id, null)
        }
    }



    override fun onPause() {
        super.onPause()

    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onDispatchTouchEvent() {}

    override fun onHandleBackPressed() {
        getParentActivity().onHandleBackPressed()
    }

    override fun getParentActivity(): IBaseActivity<*> {
        return requireActivity() as BaseActivity<*, *>
    }

    override fun getParentViewModel(): IBaseViewModel {
        return getParentActivity().getViewModel()
    }

}