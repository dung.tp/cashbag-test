package com.testapptest.test.base.view

import android.content.Intent
import com.testapptest.test.R
import com.testapptest.test.base.viewmodel.BaseViewModel
import com.testapptest.test.extension.getString

interface IBaseActivity<T : BaseViewModel> : IBaseView<T> {
    fun onChangeStatusBarColor()
    fun onFullScreen()
    fun onCheckNewIntent(intent: Intent?)
    fun getNavId(): Int?

}