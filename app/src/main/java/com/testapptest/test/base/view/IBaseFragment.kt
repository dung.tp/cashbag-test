package com.testapptest.test.base.view

import android.os.Bundle
import androidx.navigation.NavController
import com.testapptest.test.base.viewmodel.BaseViewModel
import com.testapptest.test.base.viewmodel.IBaseViewModel


interface IBaseFragment<V : BaseViewModel> : IBaseView<V> {
    val navController: NavController
    fun navigate(id: Int)
    fun navigate(id: Int, bundle: Bundle? = null)
    fun getParentActivity(): IBaseActivity<*>
    fun getParentViewModel(): IBaseViewModel
    fun onScrollToTop()
}