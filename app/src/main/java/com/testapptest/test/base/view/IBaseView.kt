package com.testapptest.test.base.view

import android.app.Activity
import android.content.Context
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.testapptest.test.base.viewmodel.BaseViewModel

interface IBaseView<T : BaseViewModel> {

    @LayoutRes
    fun getLayoutRes(): Int
    fun getModelClass(): Class<T>
    fun getViewModel(): T
    fun onDispatchTouchEvent()

    fun initView()
    fun initViewModel()
    fun showLoadingDialog()
    fun dismissLoadingDialog()
    fun onHandleBackPressed()
    fun getToolbarTitle(): String?
    fun onEditTextChangedCallback(value: String?)
    fun getCurrentFragment(id: Int): BaseFragment<*, *>?
    fun getFragments(id: Int): MutableList<Fragment>?
    val viewContext: Context
}