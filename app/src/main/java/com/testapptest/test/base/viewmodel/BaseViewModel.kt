package com.testapptest.test.base.viewmodel

//import com.google.firebase.iid.FirebaseInstanceId
import androidx.lifecycle.*
import com.google.gson.Gson
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.schedulers.Schedulers


abstract class BaseViewModel : ViewModel(), IBaseViewModel,
    LifecycleObserver {

    protected var compositeDisposable = CompositeDisposable()

    private var currentDisposable: Disposable? = null



    protected var isUserVisible: Boolean = false

    init {

    }


    override fun addDisposable(disposable: Disposable, isSaveDisposable: Boolean) {
        if (isSaveDisposable) {
            currentDisposable = disposable
        }
        if (compositeDisposable.isDisposed) {
            compositeDisposable = CompositeDisposable()
        }
        compositeDisposable.add(disposable)
    }

    open fun onRemoveCurrentDisposable() {
        currentDisposable?.apply {
            compositeDisposable.remove(this)
        }
    }

    override fun requestPermission(
        rxPermissions: RxPermissions,
        permissions: Array<String>,
        onSuccess: ((Boolean) -> Unit)?
    ) {
        rxPermissions.requestEachCombined(*permissions)
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe { granted ->
                onSuccess?.invoke(granted.granted)
            }?.apply {

            }
    }

    override fun checkPermission(
        rxPermissions: RxPermissions,
        permissions: Array<String>
    ): Boolean {
        permissions.forEach {
            if (!rxPermissions.isGranted(it)) {
                return false
            }
        }

        return true
    }



    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    override fun onCreate() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    override fun onStart() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    override fun onResume() {
        isUserVisible = true
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    override fun onPause() {
        isUserVisible = false
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    override fun onStop() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    override fun onDestroy() {
        if (!compositeDisposable.isDisposed) compositeDisposable.dispose()
    }

}