package com.testapptest.test.base.viewmodel

import androidx.lifecycle.LiveData

interface IBaseLoadMoreViewModel<T> : IBaseViewModel {
    val totalCountObs: LiveData<Int>
    val isNewList: LiveData<Boolean>
    val listObs: LiveData<MutableList<T>>
    fun loadData(isRefreshed: Boolean = false, startKey: String? = null, isLoggedIn: Boolean = true)
    fun onReloadList()
    fun lastItem(): String
    fun isFirstPage(): Boolean
}