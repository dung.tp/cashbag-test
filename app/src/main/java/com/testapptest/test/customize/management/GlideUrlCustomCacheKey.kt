package com.testapptest.test.customize.manager

import com.bumptech.glide.load.model.GlideUrl


class GlideUrlCustomCacheKey(
    url: String
) : GlideUrl(url) {

    override fun getCacheKey(): String? {
        val url = toStringUrl()
        return if (url.contains("?")) {
            url.substring(0, url.lastIndexOf("?"))
        } else {
            url
        }
    }
}