package com.testapptest.test.di

import com.testapptest.test.base.viewmodel.EmptyViewModel
import com.testapptest.test.base.viewmodel.IBaseViewModel
import dagger.Binds
import dagger.Module

@Module
abstract class ConfigureModule {

    @Binds
    abstract fun provideTemplateViewModel(viewModel: EmptyViewModel): IBaseViewModel
}