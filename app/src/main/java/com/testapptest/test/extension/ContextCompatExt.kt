package com.testapptest.test.extension

import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.testapptest.test.application.MyApp


fun Int.getString(): String {
    return MyApp.instance.getString(this)
}

fun Int.getColor(): Int {
    return ContextCompat.getColor(MyApp.instance.applicationContext, this)
}

fun Int.getDrawable(): Drawable? {
    return ContextCompat.getDrawable(MyApp.instance.applicationContext, this)
}