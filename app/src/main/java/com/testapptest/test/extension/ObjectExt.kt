package com.testapptest.test.extension

import com.google.gson.Gson



fun <T : Any> Any.copyObject(inputClass: Class<T>): T {
    return Gson().fromJson<T>(Gson().toJson(this), inputClass)
}
