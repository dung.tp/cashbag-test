package com.testapptest.test.extension

import android.annotation.SuppressLint
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.bekawestberg.loopinglayout.library.LoopingLayoutManager
import com.google.android.flexbox.*
import com.testapptest.test.R
import com.testapptest.test.customize.listener.DividerWithoutLastItemDecorator
import com.testapptest.test.customize.management.CenterLayoutManager
import com.testapptest.test.customize.management.GridSpacingItemDecoration

fun RecyclerView.initFleBox(justifyContent: Int = JustifyContent.FLEX_START) {
    FlexboxLayoutManager(context).apply {
        this.flexDirection = FlexDirection.ROW
        this.justifyContent = justifyContent
        this.flexWrap = FlexWrap.WRAP
        layoutManager = this
    }
}

fun RecyclerView.initLinear(type: Int = RecyclerView.HORIZONTAL) {
    this.layoutManager = LinearLayoutManager(this.context, type, false)
}

fun RecyclerView.initLoopingLayout() {
    this.layoutManager = LoopingLayoutManager(
        this.context,                           // Pass the context.
        LoopingLayoutManager.VERTICAL,  // Pass the orientation. Vertical by default.
        false                           // Pass whether the views are laid out in reverse.
        // False by default.
    )
}

fun RecyclerView.initGrid(size: Int) {
    this.layoutManager = GridLayoutManager(this.context, size)
}

fun RecyclerView.initSnapHelper(): LinearSnapHelper {
    val snapHelper = LinearSnapHelper().apply {  }
    onFlingListener = null
    snapHelper.attachToRecyclerView(this)
    return snapHelper
}

fun RecyclerView.findFirstVisible(): Int {
    return (layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
}

fun RecyclerView.findLastVisible(): Int {
    return (layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
}

fun RecyclerView.initCenterScroll(orientation: Int = RecyclerView.HORIZONTAL) {
    this.layoutManager = CenterLayoutManager(this.context, orientation, false)
}

fun RecyclerView.onLoadMore(onLoadMore: () -> Unit) {
    this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if (!recyclerView.canScrollVertically(1)) {
                onLoadMore.invoke()
            }
        }
    })
}

fun RecyclerView.onRevertLoadMore(onLoadMore: () -> Unit) {
    this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if (!recyclerView.canScrollVertically(-1)) {
                onLoadMore.invoke()
            }
        }
    })
}

@SuppressLint("ClickableViewAccessibility")
fun RecyclerView.initTouchable() {
    setOnTouchListener { view, motionEvent ->
        view.parent.requestDisallowInterceptTouchEvent(true)
        view.onTouchEvent(motionEvent)
        true
    }
}

fun RecyclerView.onScrollChanged(onScrollChanged: (position: Int) -> Unit) {
    addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            val layoutManager = recyclerView.layoutManager
            val positionCenter =
                (layoutManager as LinearLayoutManager).findFirstVisibleItemPosition() + 1
            Log.d("onScrollChanged", "positionView $positionCenter")
            onScrollChanged.invoke(positionCenter)
        }

    })
}

fun RecyclerView.onLoopingScrollChanged(onScrollChanged: (position: Int) -> Unit) {
    addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            val layoutManager = recyclerView.layoutManager
            val positionFirst =
                (layoutManager as LoopingLayoutManager).findFirstVisibleItemPosition()
            val positionLast = (layoutManager as LoopingLayoutManager).findLastVisibleItemPosition()

            val visibleChild = layoutManager.getChildAt(1)
            val positionCenter = visibleChild?.let { recyclerView.getChildAdapterPosition(it) }

            Log.d("onLoopingScrollChanged", "positionFirst $positionFirst")
            Log.d("onLoopingScrollChanged", "positionCenter $positionCenter")
            Log.d("onLoopingScrollChanged", "positionLast $positionLast")
            if (positionCenter != null) {
                onScrollChanged.invoke(positionCenter)
            } else {
                onScrollChanged.invoke(positionFirst)
            }
        }

    })
}

fun RecyclerView.addDivider(padding: Int = 0) {
    val dividerDrawable = ContextCompat.getDrawable(context, R.drawable.divider)
    dividerDrawable?.let {
        this.addItemDecoration(
            DividerWithoutLastItemDecorator(it, padding.convertDpToPx())
        )
    }
}

fun RecyclerView.addItemSpacing(space: Int) {
    this.addItemDecoration(GridSpacingItemDecoration(1, space.dp, true))
}