package com.testapptest.test.extension

import androidx.viewpager.widget.ViewPager



fun ViewPager.onPageChange(onChanged: (Int) -> Unit) {
    this.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) {
        }

        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
        }

        override fun onPageSelected(position: Int) {
            onChanged.invoke(position)
        }
    })
}