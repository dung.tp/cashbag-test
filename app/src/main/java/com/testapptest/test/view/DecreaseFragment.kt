package com.testapptest.test.view

import android.util.Log
import com.testapptest.test.R
import com.testapptest.test.base.view.BaseFragment
import com.testapptest.test.databinding.FragmentDecreaseBinding
import com.testapptest.test.extension.getDefault
import com.testapptest.test.viewmodel.DecreaseViewModel

class DecreaseFragment : BaseFragment<FragmentDecreaseBinding, DecreaseViewModel>() {
    override fun getLayoutRes(): Int {
        return R.layout.fragment_decrease
    }

    override fun getModelClass(): Class<DecreaseViewModel> {
        return  DecreaseViewModel::class.java
    }

    override fun initViewModel() {
        mViewModel.apply {
            val number = arguments?.getInt("key").getDefault()
            setNumber(number)
        }
    }

    override fun initView() {
        super.initView()

        dataBinding.apply {
            iView = this@DecreaseFragment
        }
    }

    fun decrease(){
        mViewModel.decreaseNumber()
    }

    fun back(){
        onHandleBackPressed()
    }

    override fun onHandleBackPressed() {
        super.onHandleBackPressed()
        (activity as MainActivity).setNumber(mViewModel.number.value.getDefault())
    }

}