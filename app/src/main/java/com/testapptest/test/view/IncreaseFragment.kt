package com.testapptest.test.view

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import com.testapptest.test.R
import com.testapptest.test.base.view.BaseFragment
import com.testapptest.test.databinding.FragmentIncreaseBinding
import com.testapptest.test.extension.getDefault
import com.testapptest.test.viewmodel.IncreaseViewModel
import com.testapptest.test.viewmodel.MainViewModel
class IncreaseFragment : BaseFragment<FragmentIncreaseBinding, IncreaseViewModel>() {
    override fun getLayoutRes(): Int {
        return R.layout.fragment_increase
    }

    override fun getModelClass(): Class<IncreaseViewModel> {
        return  IncreaseViewModel::class.java
    }

    override fun initView() {
        super.initView()
        dataBinding.apply {
            Log.d("IncreaseFragment","initView")
            iView = this@IncreaseFragment
        }
    }
    override fun initViewModel() {
        mViewModel.apply {
            Log.d("IncreaseFragment","initViewModel")
            val num = (activity as MainActivity).getNumber()
            setNumber(num)
        }
    }

    fun increase(){
        mViewModel.increaseNumber()
    }

    fun send(){
        navController.navigate(R.id.action_increaseFragment_to_decreaseFragment, Bundle().apply {
            putInt("key", mViewModel.number.value.getDefault())
        })
    }

}
