package com.testapptest.test.view

import android.util.Log
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.testapptest.test.R
import com.testapptest.test.application.MyApp
import com.testapptest.test.base.view.BaseActivity
import com.testapptest.test.databinding.ActivityMainBinding
import com.testapptest.test.extension.addDivider
import com.testapptest.test.extension.dp
import com.testapptest.test.extension.getDefault
import com.testapptest.test.extension.initLinear
import com.testapptest.test.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    override fun getLayoutRes(): Int = R.layout.activity_main
    override fun getModelClass(): Class<MainViewModel> {
        return MainViewModel::class.java
    }

    override fun initView() {
        super.initView()
        dataBinding.apply {

        }
    }

    override fun initViewModel() {
        super.initViewModel()
        mViewModel.apply {
        }
    }

    fun getNumber(): Int {
        Log.d("MainActivity", "getNumber ${mViewModel.number.value.getDefault()}")
        return mViewModel.number.value.getDefault()
    }

    fun setNumber(num: Int){
        Log.d("MainActivity", "setNumber ${num}")
        mViewModel.setNumber(num)
    }


}