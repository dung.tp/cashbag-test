package com.testapptest.test.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.testapptest.test.base.viewmodel.BaseViewModel
import javax.inject.Inject

class DecreaseViewModel @Inject constructor(
    private val context: Context
): BaseViewModel(){
    private val _number = MutableLiveData<Int>(0)
    val number : LiveData<Int>
        get() {
            return _number
        }

    fun decreaseNumber(){
        _number.postValue(number.value?.minus(1))
    }

    fun setNumber(num: Int){
        _number.value = num
    }
}