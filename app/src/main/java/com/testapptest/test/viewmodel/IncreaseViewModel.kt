package com.testapptest.test.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.testapptest.test.base.viewmodel.BaseViewModel
import javax.inject.Inject

class IncreaseViewModel @Inject constructor(): BaseViewModel(){

    private val _number = MutableLiveData<Int>(0)
    val number : LiveData<Int>
        get() {
            return _number
        }

    fun increaseNumber(){
        _number.postValue(number.value?.plus(1))
    }

    fun setNumber(num: Int){
        _number.postValue(num)
    }
}