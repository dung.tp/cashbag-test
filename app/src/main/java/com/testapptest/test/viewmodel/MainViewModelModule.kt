package com.testapptest.test.viewmodel

import androidx.lifecycle.ViewModel
import com.testapptest.test.di.scope.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class MainViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(IncreaseViewModel::class)
    abstract fun bindIncreaseViewModel(viewModel: IncreaseViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DecreaseViewModel::class)
    abstract fun bindDecreaseViewModel(viewModel: DecreaseViewModel): ViewModel
}